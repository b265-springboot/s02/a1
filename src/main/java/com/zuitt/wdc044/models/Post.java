package com.zuitt.wdc044.models;

import javax.persistence.*;

//mark this Java Object as a representation of a database table via @Entity
@Entity
//designate table name via @Table
@Table(name="posts")
public class Post {

	//indicate that this property represents the primary key via @Id
	@Id
	//values for this property will be auto-incremented
	@GeneratedValue
	private Long id;

	@Column
	private String title;

	@Column
	private String content;

	//represents the many side of the relationship
	@ManyToOne
	//to reference the foreign key column
	@JoinColumn(name = "user_id", nullable=false)
	private User user;

	public Post(){}

	public Post(String title, String content) {
		this.title = title;
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return  content;
	}

	public void setContent(String content){
		this.content = content;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user){
		this.user = user;
	}

}
