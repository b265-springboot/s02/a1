package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class PostController {
	@Autowired
	PostService postService;

	// create a new post
	@RequestMapping(value = "/posts", method = RequestMethod.POST)
	public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){

		postService.createPost(stringToken, post);
		return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
	}

	@GetMapping(value ="/posts")
	public ResponseEntity<List<Post>> getAllPosts() {
		List<Post> posts = (List<Post>) postService.getPosts();
		return new ResponseEntity<>(posts, HttpStatus.OK);
	}

	//Edit a post
	@RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
	//@PathVariable is used for data pass in URL
	//@PathVariable is used to retrieve exact record
	public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value= "Authorization") String stringToken, @RequestBody Post post) {
		return postService.updatePost(postId,stringToken,post);
	}

	// deleting a post
	@RequestMapping(value = "/post/{postId}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value= "Authorization") String stringToken){
		return postService.deletePost(postId,stringToken);
	};

	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	public ResponseEntity<Object> getMypost(@RequestHeader(value= "Authorization") String stringToken){
		return new ResponseEntity<>(postService.getMypost(stringToken), HttpStatus.OK);
	};



}
