package com.zuitt.wdc044.services;

import com.zuitt.wdc044.config.JwtToken;
import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import com.zuitt.wdc044.repositories.PostRepository;
import com.zuitt.wdc044.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class PostServiceImpl implements PostService {
	@Autowired
	private PostRepository postRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	JwtToken jwtToken;

	public void createPost(String stringToken, Post post){
		User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));

		Post newPost = new Post();
		//Title and content will come from
		newPost.setTitle(post.getTitle());
		newPost.setContent(post.getContent());
		newPost.setUser(author);

		// actual saving of post
		postRepository.save(newPost);

	}

	public Iterable<Post> getPosts() {
		return postRepository.findAll();
	}




	public ResponseEntity updatePost(Long id, String stringToken, Post post) {

		Post postForUpdating = postRepository.findById(id).get();
		String postAuthor = postForUpdating.getUser().getUsername();

		String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);

		if (authenticatedUser.equals(postAuthor)) {

			postForUpdating.setTitle(post.getTitle());
			postForUpdating.setContent(post.getContent());

			postRepository.save(postForUpdating);

			return new ResponseEntity<>("Post updated successfully", HttpStatus.OK);

		} else {
			return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
		}
	}

	public ResponseEntity deletePost(Long id, String stringToken){
		Post postForUpdating = postRepository.findById(id).get();

		String postAuthor = postForUpdating.getUser().getUsername();

		String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
		if (authenticatedUser.equals(postAuthor)){
			postRepository.deleteById(id);

			return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);

		} else{
			return new ResponseEntity<>("You are not authorized to edit this post", HttpStatus.UNAUTHORIZED);
		}
	}

	public Set<Post> getMypost(String stringToken){
		
		String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
		User user = userRepository.findByUsername(authenticatedUser);

		return user.getPosts();



	}

//	public Iterable<Post> getMypost (String stringToken){
//
//
//
//
//		String authenticatedUser = jwtToken.getUsernameFromToken(stringToken);
//		User user = userRepository.findByUsername(authenticatedUser);
//
//		if (user != null){
//
//			return postRepository.findAll();
//
//		} else{
//			return (Iterable<Post>) new ResponseEntity<>("You are not authorized to view posts", HttpStatus.UNAUTHORIZED);
//		}
//
//	}

}