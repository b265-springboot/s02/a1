package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;

import java.util.Optional;

//This interface will be used to register a user via userController.
public interface UserService {

	//registration of a user
	void createUser(User user);

	//checking if user exists
	Optional<User> findByUsername(String username);

}
